from selenium import webdriver
from bs4 import BeautifulSoup as bs
import time
from os import getenv
from dotenv import load_dotenv


def main():
    load_dotenv()
    executable_path = getenv('PHATOM_JS_PATH', None)

    driver = webdriver.PhantomJS(executable_path=executable_path)
    driver.get(url='http://pythonscraping.com/pages/javascript/ajaxDemo.html')
    time.sleep(3)
    soup = bs(driver.page_source, features='html.parser')
    text = soup.find('div', {'id': 'content'}).get_text()  # forward PhantomJS source to BeautifulSoup
    print(text)
    text = driver.find_element_by_id(id_='content').text  # Simple selector by PhantomJS
    print(text)
    driver.close()


if __name__ == '__main__':
    main()
