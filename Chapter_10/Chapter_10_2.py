from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as WDW
from selenium.webdriver.support import expected_conditions as EC
from os import getenv
from dotenv import load_dotenv

recursion_level = 0


def get_element(driver, timeout):
    global recursion_level
    recursion_level += 1
    print(f'Recursion level: {recursion_level}')
    if recursion_level > 2:
        raise Exception('max recursion')
    else:
        print(timeout)
        element = WDW(driver=driver, timeout=timeout).until(EC.presence_of_element_located((By.ID, 'loadedButton')))


def main():
    load_dotenv()
    executable_path = getenv('PHATOM_JS_PATH', None)
    """
    This function try to find presence of element on page, if it not exists grow timeout and recursion level
    :return:
    """
    driver = webdriver.PhantomJS(executable_path=executable_path)
    driver.get(url='http://pythonscraping.com/pages/javascript/ajaxDemo.html')
    timeout = 0.25
    try:
        get_element(driver=driver, timeout=timeout)
    except Exception:
        get_element(driver=driver, timeout=timeout+0.25)
    finally:
        print(driver.find_element_by_id('content').text)
        driver.close()


if __name__ == '__main__':
    main()
