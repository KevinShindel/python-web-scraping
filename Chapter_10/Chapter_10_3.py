from selenium.webdriver import PhantomJS as pj
from selenium.webdriver.common.by import By
import time
from selenium.common.exceptions import StaleElementReferenceException as se
from os import getenv
from dotenv import load_dotenv


def await_pj(driver: pj):
    """
    try to find element if cant repeat after 0.5sec
    :param driver:
    :return:
    """
    el = driver.find_element(By.TAG_NAME, 'html')
    count = 0
    while True:
        count += 1
        if count > 10:
            print('Timeout after 10 retries and returning')
            return
        time.sleep(0.5)
        try:
            el == driver.find_element(By.TAG_NAME, 'html')
        except se:
            return


def main():
    load_dotenv()
    executable_path = getenv('PHATOM_JS_PATH', None)
    """
    This function bypass client-side redirect
    :return:
    """
    driver = pj(executable_path=executable_path)
    driver.get(url='http://pythonscraping.com/pages/javascript/redirectDemo1.html')
    await_pj(driver=driver)
    print(driver.page_source)
    driver.close()


if __name__ == '__main__':
    main()
