from urllib.request import urlopen

from bs4 import BeautifulSoup as bs


def main():
    text_page = urlopen('http://forums.linuxmint.com/')
    soup = bs(text_page, features='html.parser')
    encoding = soup.find(lambda tag: tag.name == 'meta' and tag.get('charset'))
    encoding = encoding.get('charset')
    print(str(text_page.read(), encoding=encoding))


if __name__ == '__main__':
    main()

