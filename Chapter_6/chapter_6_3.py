from io import BytesIO
from urllib.request import urlopen
from PyPDF2 import PdfFileReader


def read_pdf():
    url = "http://pythonscraping.com/pages/warandpeace/chapter1.pdf"
    remote_file = urlopen(url).read()
    memory_file = BytesIO(remote_file)
    pdf_file = PdfFileReader(memory_file)

    for pageNum in range(pdf_file.getNumPages()):
        print(f'page number: {pageNum} \n')
        current_page = pdf_file.getPage(pageNum)
        print(current_page.extractText())


if __name__ == '__main__':
    read_pdf()
