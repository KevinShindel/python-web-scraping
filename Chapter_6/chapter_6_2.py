from csv import DictReader
from io import StringIO
from urllib.request import urlopen


def main():
    data = urlopen('http://pythonscraping.com/files/MontyPythonAlbums.csv').read().decode('ascii', 'ignore')
    data_file = StringIO(data)
    csw_reader = DictReader(data_file)
    print([row for row in csw_reader])


if __name__ == '__main__':
    main()
