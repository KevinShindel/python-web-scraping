from io import BytesIO
from urllib.request import urlopen
from zipfile import ZipFile
from bs4 import BeautifulSoup as bs


def read_word(remote_file):
    """
    convert docx to txt
    :param remote_file:
    :return:
    """
    memory_file = BytesIO(remote_file)
    doc = ZipFile(memory_file)
    xml_content = doc.read('word/document.xml').decode('utf-8')
    text = bs(xml_content, features='lxml')
    for el in text.find_all('w:t'):
        print(el.get_text())


if __name__ == '__main__':
    url = "http://pythonscraping.com/pages/AWordDocument.docx"
    remote_file = urlopen(url).read()
    read_word(remote_file)
