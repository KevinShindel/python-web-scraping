from fake_useragent import UserAgent as UA
from requests import Session
from bs4 import BeautifulSoup as bs


def main():
    """
    Bypass bot detection via headers
    :return:
    """
    url = 'https://whatismybrowser.com'
    headers = {'user-Agent': UA().random}
    html = Session().get(url=url, headers=headers)
    soup = bs(html.text, features='html.parser')
    data = soup.find('a', title='Your User Agent').get_text()
    print(f'Faked headers: {headers["user-Agent"]} \n detected headers: {data}')


if __name__ == '__main__':
    main()

