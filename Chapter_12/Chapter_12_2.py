from selenium import webdriver
from selenium.webdriver.common.by import By
from os import getenv
from dotenv import load_dotenv


def main():
    load_dotenv()
    executable_path = getenv('PHATOM_JS_PATH', None)
    """
    This method check for visiability of elements on page for avoid ban
    :return:
    """
    url = 'http://pythonscraping.com/pages/itsatrap.html'
    driver = webdriver.PhantomJS(executable_path=executable_path,
                                 )
    driver.get(url=url)
    links = driver.find_elements(By.TAG_NAME, 'a')
    for link in links:
        if not link.is_displayed():
            print(f'Link: {link.get_attribute("href")} is not visible, it is a trap.')
        else:
            print(f'Link: {link.get_attribute("href")} is visible!')

    fields = driver.find_elements(By.TAG_NAME, 'input')
    for field in fields:
        if not field.is_displayed():
            print(f'Field: {field.get_attribute("name")} is not visible, it is a trap.')
        else:
            print(f'Field: {field.get_attribute("name")} is visible!')
    driver.close()


if __name__ == '__main__':
    main()
