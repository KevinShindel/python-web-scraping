from bs4 import BeautifulSoup as bs
from urllib.request import urlopen
import unittest
import re
import random
from urllib.parse import unquote


class TestWiki(unittest.TestCase):
    soup: bs = None
    url: str = None

    def test_PageProperties(self):
        global soup, url
        url = 'http://en.wikipedia.org/wiki/Monty_Python'
        for i in range(1, 100):
            html = urlopen(url)
            soup = bs(html, features='html.parser')
            titles = self.titleMatchesURL()
            self.assertEqual(titles[0], titles[1])
            self.assertTrue(self.contentExists())
            url = self.getNextLink()
        print('done')

    def titleMatchesURL(self):
        global soup, url
        pageTitle = soup.find('h1').get_text()
        urlTitle = url[(url.index('/wiki/')+6):]
        urlTitle = urlTitle.replace('_', ' ')
        urlTitle= unquote(urlTitle)
        return [pageTitle.lower(), urlTitle.lower()]

    def contentExists(self):
        global soup
        content = soup.find('div', {'id': 'mw-content-text'})
        return bool(content)

    def getNextLink(self):
        global soup
        links = soup.find('div', {'id': 'bodyContent'}).find_all('a', href=re.compile('^(/wiki/)((?!:).)*$'))
        randomLink = random.SystemRandom().choice(links)
        return 'https://wikipedia.org{}'.format(randomLink.attrs['href'])


if __name__ == '__main__':
    unittest.main()
