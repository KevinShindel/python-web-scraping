from urllib.request import urlopen
import unittest
from bs4 import BeautifulSoup as bs


class TestWiki(unittest.TestCase):
    soup = None

    @classmethod
    def setUpClass(cls) -> None:
        global soup
        url = 'http://en.wikipedia.org/wiki/Monty_Python'
        html = urlopen(url)
        soup = bs(html, features='html.parser')

    def test_title(self):
        global soup
        title = soup.find('h1').get_text()
        self.assertEqual('Monty Python', title)

    def test_content_exist(self):
        global soup
        content = soup.find('div', id='mw-content-text')
        self.assertIsNotNone(content)


if __name__ == '__main__':
    unittest.main()
