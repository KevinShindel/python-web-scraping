from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from os import getenv
from dotenv import load_dotenv


def main():
    load_dotenv()
    executable_path = getenv('PHATOM_JS_PATH', None)

    url = 'http://pythonscraping.com/pages/files/form.html'
    driver = webdriver.PhantomJS(executable_path=executable_path)
    driver.get(url)
    # find fields
    first_field = driver.find_element(By.NAME, 'firstname')
    last_field = driver.find_element(By.NAME, 'lastname')
    # create actions chain
    actions = ActionChains(driver).click(first_field).send_keys('Ryan').\
        click(last_field).send_keys('Mitchel').\
        send_keys(Keys.RETURN)
    # perform actions
    actions.perform()

    body = driver.find_element(By.TAG_NAME, 'body').text
    print(body)
    driver.close()


if __name__ == '__main__':
    main()
