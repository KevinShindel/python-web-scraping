import unittest


class TestAddition(unittest.TestCase):

    def setUp(self) -> None:
        print('Setting up a test')

    def tearDown(self) -> None:
        print('Tearing down a test')

    def test_two_plus_two(self):
        total = 2+2
        self.assertEqual(4, total)


if __name__ == '__main__':
    unittest.main()
