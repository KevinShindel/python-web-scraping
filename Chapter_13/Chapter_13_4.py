from selenium import webdriver
from os import getenv
from dotenv import load_dotenv


def main():
    load_dotenv()
    executable_path = getenv('PHATOM_JS_PATH', None)

    url = 'http://en.wikipedia.org/wiki/Monty_Python'
    driver = webdriver.PhantomJS(executable_path=executable_path)
    driver.get(url)
    assert 'Monty Python' in driver.title
    driver.close()


if __name__ == '__main__':
    main()
