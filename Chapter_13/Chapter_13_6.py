from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from os import getenv
from dotenv import load_dotenv


def main():
    load_dotenv()
    executable_path = getenv('CHROME_DRIVER_PATH', None)

    url = 'http://bit.ly/1GP52py'
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(executable_path=executable_path, options=chrome_options)
    driver.get(url)
    message = driver.find_element(By.ID, 'message').text
    print(message)
    draggable = driver.find_element(By.ID, 'draggable')
    target = driver.find_element(By.ID, 'div2')

    actions = ActionChains(driver)

    # Perform moving by offset
    # x = draggable.location['x']
    # y = target.location['y']
    # .click_and_hold(draggable).move_by_offset(x, y-50).release(draggable)
    #     drag_and_drop(draggable, target)
    #     drag_and_drop_by_offset(xoffset=x+10, yoffset=y-100, source=draggable)


    # Perform by drag and drop
    actions.drag_and_drop(draggable, target)

    actions.perform()
    message = driver.find_element(By.ID, 'message').text
    print(message)
    driver.close()


if __name__ == '__main__':
    main()
