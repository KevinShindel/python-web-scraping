import datetime
import random
import re

from bs4 import BeautifulSoup
from urllib.request import urlopen

url = '/wiki/Museum_Management_and_Curatorship'
random.seed(datetime.datetime.now())
pages = set()


def main(link: str = None):
    global pages
    local_url = 'http://en.wikipedia.org'+link
    request = urlopen(url=local_url)
    html = BeautifulSoup(request, features='html.parser')
    try:
        print(html.h1.get_text())
        print(html.find(id='mw-content-text').find('p'))
        print(html.find(id='ca-edit').find('span').find('a').attrs['href'])
    except AttributeError:
        print('not found')
    for link in html.find_all('a', {'href': re.compile(r'^(/wiki/)((?!:).)*$')}):
        if 'href' in link.attrs and link.attrs['href'] not in pages:
            new_page = link['href']
            print(new_page)
            pages.add(new_page)
            main(link=new_page)


if __name__ == '__main__':
    main(link='')
