import datetime
import random
import re

from bs4 import BeautifulSoup
from urllib.request import urlopen

url = '/wiki/Museum_Management_and_Curatorship'
random.seed(datetime.datetime.now())


def main(link: str = None):
    local_url = 'http://en.wikipedia.org'+link
    request = urlopen(url=local_url)
    html = BeautifulSoup(request, features='html.parser')
    return html.find('div', {'id': 'bodyContent'}).find_all('a', {'href': re.compile(r'^(/wiki/)((?!:).)*$')})


if __name__ == '__main__':
    links = main(link=url)
    while len(links) > 0:
        new_article = links[random.randint(0, len(links)-1)].attrs['href']
        print(new_article)
        links = main(link=new_article)
