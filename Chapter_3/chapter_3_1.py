import re

from bs4 import BeautifulSoup
from urllib.request import urlopen
url = 'http://en.wikipedia.org/wiki/Kevin_Bacon'


def main(link: str = None):
    request = urlopen(url=link)
    html = BeautifulSoup(request, features='html.parser')
    for item in html.find('div', {'id': 'bodyContent'}).find_all('a', {'href': re.compile(r'^(/wiki/)((?!:).)*$')}):
        print(item)


if __name__ == '__main__':
    main(link=url)
