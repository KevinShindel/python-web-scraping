import scrapy

from ..items import Article


class ArticleSpider(scrapy.Spider):
    name = 'article'
    allowed_domains = ['en.wikipedia.org']
    start_urls = ['https://en.wikipedia.org/wiki/Main_Page']

    def parse(self, response):
        item = Article()
        title = response.xpath('//h1/text()')[0].extract()
        print(f'title: {title}')
        item['title'] = title
        return item
