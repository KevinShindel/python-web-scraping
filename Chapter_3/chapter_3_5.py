import datetime
import random
import re
from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.parse import urlparse

random.seed(datetime.datetime.now())
pages = set()


def get_internal_links(html: BeautifulSoup, url: str) -> [str]:
    url = f'{urlparse(url).scheme}://{urlparse(url).netloc}'
    internal_links = []
    for link in html.find_all('a', {'href': re.compile(f'^(/|.*'+url+')')}):
        if link.attrs['href'] is not None and link.attrs['href'] not in internal_links:
            if link.attrs['href'].startswith('/'):
                internal_links.append(url+link.attrs['href'])
            else:
                internal_links.append(link.attrs['href'])
    return internal_links


def get_external_links(html: BeautifulSoup, url: str) -> [str]:
    external_links = []
    for link in html.find_all('a', {'href': re.compile(r'^(http|https|www)((?!'+url+').)*$')}):
        if link.attrs['href'] is not None and link.attrs['href'] not in external_links:
            external_links.append(link.attrs['href'])
    return external_links


def get_random_external_link(url: str) -> str:
    request = urlopen(url)
    html = BeautifulSoup(request, features='html.parser')
    external_links = get_external_links(html=html, url=urlparse(url).netloc)
    if len(external_links) == 0:
        print('No external links')
        domain = f'{urlparse(url).scheme}://{urlparse(url).netloc}'
        internal_links = get_internal_links(html=html, url=domain)
        return get_random_external_link(internal_links[random.randint(0, len(internal_links)-1)])
    else:
        return external_links[random.randint(0, len(external_links)-1)]


def follow_external(url: str):
    external_link = get_random_external_link(url=url)
    print(f'Random external link: {external_link}')
    follow_external(external_link)


if __name__ == '__main__':
    follow_external(url='http://oreilly.com')
