import pymysql


class SolutionFound(RuntimeError):
    def __init__(self, message):
        self.message = message


class BreadthSearch:

    def __init__(self):
        self.conn = pymysql.connect(host='127.0.0.1',
                                    user='username',
                                    password='password',
                                    db='mysql')
        self.cur = self.conn.cursor()
        self.cur.execute('USE wikipedia')

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cur.close()
        self.conn.close()

    def get_links(self, from_page_id):
        self.cur.execute(f"SELECT to_page_id FROM links WHERE from_page_id = {from_page_id}")
        if self.cur.rowcount == 0:
            return None
        else:
            return [x[0] for x in self.cur.fetchall()]

    def construct_dict(self, curr_page_id):
        links = self.get_links(from_page_id=curr_page_id)
        if links:
            return dict(zip(links, [{}]*len(links)))
        else:
            return {}

    def search_depth(self, target_page_id, current_page_id, link_tree, depth):
        if depth == 0:
            return link_tree
        else:
            if not link_tree:
                link_tree = self.construct_dict(curr_page_id=current_page_id)
                if not link_tree:
                    return {}
            if target_page_id in link_tree.keys():
                print(f'TARGET {target_page_id} FOUND')
                raise SolutionFound(message=current_page_id)
            for branch_key, branch_value in link_tree.items():
                try:
                    link_tree[branch_key] = self.search_depth(target_page_id=target_page_id,
                                                              current_page_id=current_page_id,
                                                              link_tree=branch_value, depth=depth-1)
                except SolutionFound as Err:
                    print(Err.message)
                    raise SolutionFound(message=current_page_id)
            return link_tree


if __name__ == '__main__':
    try:
        BreadthSearch().search_depth(target_page_id=770, current_page_id=9967, link_tree={}, depth=4)
        print('SOLUTION NOT FOUND')
    except SolutionFound as Err:
        print(Err.message)
