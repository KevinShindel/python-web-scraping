from urllib.request import urlopen
from random import randint
import string


def wordlist_sum(wordlist: dict):
    item_sum = 0
    for word, value in wordlist.items():
        item_sum += value
    return item_sum


def retrieve_word(wordlist: dict):
    rand_index = randint(1, wordlist_sum(wordlist))
    for word, value in wordlist.items():
        rand_index -= value
        if rand_index <= 0:
            return word


def build_wordlist(text: str):
    text = text.replace('\n', ' ')
    text = text.replace(r'\"', '')
    for symbol in string.punctuation:
        text.replace(symbol, ' '+symbol+' ')
    words = text.split(' ')
    words = [word for word in words if word != '']
    word_dict = {}
    for i in range(1, len(words)):
        if words[i-1] not in word_dict:
            word_dict[words[i-1]] = {}
        if words[i] not in word_dict[words[i-1]]:
            word_dict[words[i-1]][words[i]] = 0
        word_dict[words[i-1]][words[i]] = word_dict[words[i-1]][words[i]]+1
    return word_dict


def main():
    content = urlopen('http://pythonscraping.com/files/inaugurationSpeech.txt').read().decode('utf-8')
    word_dict = build_wordlist(content)
    length = 200
    chain = ''
    current_word = 'I'
    for i in range(0, length):
        chain += current_word + ' '
        current_word = retrieve_word(word_dict[current_word])
    print(chain)


if __name__ == '__main__':
    main()
