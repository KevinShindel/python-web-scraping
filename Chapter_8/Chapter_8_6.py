from nltk import word_tokenize, sent_tokenize, pos_tag


def main():
    sentence = sent_tokenize("Google is one of the most best companies of the world. "
                             "I constantly google myself to see what i'm up to. "
                             "I try to Google some in internet")
    noums = ['NN', 'NNS', 'NNP', 'NNPS']
    for sent in sentence:
        if 'google' in sent.lower():
            tagged_word = pos_tag(word_tokenize(sent))
            for word in tagged_word:
                if word[0].lower() == 'google' and word[1] in noums:
                    print(sent)


if __name__ == '__main__':
    main()
