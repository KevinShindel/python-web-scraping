import operator
from re import sub
from string import punctuation, digits
from urllib.request import urlopen


def clean_input(text):
    text = sub(f'[{punctuation}{digits}\n+]', '', text).lower()
    text = bytes(text, encoding='utf-8')
    text = text.decode(encoding='ascii', errors='ignore')
    otput_list = []
    for item in text:
        if len(item) > 1 or (item == 'a' or item == 'i'):
            otput_list.append(item)
    return otput_list


def ngrams(text, n):
    text = clean_input(text)
    output_data = {}
    for i in range(len(text)-n+1):
        ngram_temp = " ".join(text[i:i+n])
        if ngram_temp not in output_data:
            output_data[ngram_temp] = 0
        output_data[ngram_temp] += 1
    return output_data


def main():
    content = urlopen('http://pythonscraping.com/files/inaugurationSpeech.txt').read().decode('utf-8')
    ngram = ngrams(content, 2)
    sorted_ingram = sorted(ngram.items(), key=operator.itemgetter(1), reverse=True)
    print(sorted_ingram)


if __name__ == '__main__':
    main()
