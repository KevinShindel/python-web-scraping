from collections import Counter
from re import sub
from string import punctuation, whitespace
from urllib.request import urlopen


def clean_sentence(sentence):
    sentence = sentence.split(' ')
    sentence = [word.strip(punctuation + whitespace) for word in sentence]
    sentence = [word for word in sentence if len(word) > 1 or (word.lower() == 'a' or word.lower() == 'i')]
    return sentence


def is_common(ngram: list):
    common_words = ['THE', 'BE', 'AND', 'OF', 'A', 'IN', 'TO', 'HAVE', 'IT', 'I', 'THAT', 'FOR', 'YOU', 'HE', 'WITH',
                    'ON', 'DO', 'SAY', 'THIS', 'THEY', 'IS', 'AN', 'AT', 'BUT', 'WE', 'HIS', 'FROM', 'THAT', 'NOT',
                    'BY', 'SHE', 'OR', 'AS', 'WHAT', 'GO', 'THEIR', 'CAN', 'WHO', 'GET', 'IF', 'WOULD', 'HER', 'ALL',
                    'MY', 'MAKE', 'ABOUT', 'KNOW', 'WILL', 'AS', 'UP', 'ONE', 'TIME', 'HAS', 'BEEN', 'THERE', 'YEAR',
                    'SO', 'THINK', 'WHEN', 'WHICH', 'THEM', 'SOME', 'ME', 'PEOPLE', 'TAKE', 'OUT', 'INTO', 'JUST',
                    'SEE', 'HIM', 'YOUR', 'COME', 'COULD', 'NOW', 'THAN', 'LIKE', 'OTHER', 'HOW', 'THEN', 'ITS', 'OUR',
                    'TWO', 'MORE', 'THESE', 'WANT', 'WAY', 'LOOK', 'FIRST', 'ALSO', 'NEW', 'BECAUSE', 'DAY', 'MORE',
                    'USE', 'NO', 'MAN', 'FIND', 'HERE', 'THING', 'GIVE', 'MANY', 'WELL']
    for word in ngram:
        if word in common_words:
            return True
    return False


def clean_input(content):
    content = content.upper()
    content = sub(r'\n|[[\d+\]]', ' ', content)
    content = bytes(content, "UTF-8")
    content = content.decode("ascii", "ignore")
    sentences = content.split('. ')
    return [clean_sentence(sentence) for sentence in sentences]


def get_ngrams_from_sentence(content, n):
    output = []
    for i in range(len(content) - n + 1):
        if not is_common(content[i:i + n]):
            output.append(content[i:i + n])
    return output


def get_ngrams(content, n):
    content = clean_input(content)
    ngrams = Counter()
    ngrams_list = []
    for sentence in content:
        new_ngrams = [' '.join(ngram) for ngram in get_ngrams_from_sentence(sentence, n)]
        ngrams_list.extend(new_ngrams)
        ngrams.update(new_ngrams)
    return ngrams


def main():
    content = urlopen('http://pythonscraping.com/files/inaugurationSpeech.txt').read().decode('utf-8')
    ngram = get_ngrams(content, 2)
    print(ngram)
    print(len(ngram))


if __name__ == '__main__':
    main()
