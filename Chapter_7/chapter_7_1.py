from urllib.request import urlopen
from bs4 import BeautifulSoup as bs
from re import sub
from string import punctuation, digits
from collections import OrderedDict


def ngram(text: list, n: int) -> list:
    return [text[i:i+n] for i in range(len(text)-n+1)]


def clean_data(data: str) -> list:
    text = sub(f'[{punctuation}\n{digits}]', ' ', data)
    text = bytes(text, "utf-8")
    text = text.decode(encoding='ascii', errors='ignore')
    return [item for item in text.split(' ') if len(item) > 1]


def main():
    html = urlopen('http://en.wikipedia.org/wiki/Python_(programming_language)')
    soup = bs(html, features='html.parser')
    content = soup.find('div', {'id': 'mw-content-text'}).get_text()
    clean = clean_data(content)
    ngrams = ngram(text=clean, n=2)
    ngrams = OrderedDict(sorted(ngrams, key=lambda t: t[1], reverse=True))
    print(ngrams)
    print(len(ngrams))


if __name__ == '__main__':
    main()
