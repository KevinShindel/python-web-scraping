import requests


def main():
    """
    this method post data to simple form
    http://pythonscraping.com/pages/files/form.html
    :return:
    """
    url = 'http://pythonscraping.com/pages/files/processing.php'
    data = {'firstname': 'Ryan', 'lastname': 'Mitchell'}

    req = requests.post(url=url, data=data)
    print(req.text)


if __name__ == '__main__':
    main()
