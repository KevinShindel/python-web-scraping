from requests import get
from requests.auth import HTTPBasicAuth
from os import getenv
from dotenv import load_dotenv


def main():
    load_dotenv()
    username = getenv('API_USER_EMAIl', None)
    password = getenv('API_USER_PASSWORD', None)
    url = getenv('KIBANA_URL', None)

    """
    This function bypass base authentication [KIBANA]
    :return:
    """
    auth = HTTPBasicAuth(username=username, password=password)
    req = get(url=url, auth=auth)
    print(req.text)


if __name__ == '__main__':
    main()
