from requests import Session


def main():
    """
    This function process cookies from server
    page url: http://pythonscraping.com/pages/cookies/login.html
    :return:
    """
    session = Session()

    params = {'username': 'username', 'password': 'password'}
    s = session.get(url="http://pythonscraping.com/pages/cookies/login.html")
    print(s.text)
    print("-----------")
    s = session.post("http://pythonscraping.com/pages/cookies/welcome.php", params)
    print("Cookie is set to:")
    print(s.cookies.get_dict())
    print("-----------")
    print("Going to profile page...")
    s = session.get("http://pythonscraping.com/pages/cookies/welcome.php")
    print(s.text)


if __name__ == '__main__':
    main()
