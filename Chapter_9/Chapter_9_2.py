import requests
from os import getenv
from dotenv import load_dotenv


def main():
    load_dotenv()
    email_address = getenv('API_USER_EMAIl', None)
    password = getenv('API_USER_PASSWORD', None)
    """
    This function bypass post form authentication [IPSTACK]
    :return:
    """
    url = 'https://ipstack.com/signin'
    data = {'email_address': email_address, 'password': password}
    req = requests
    req = req.post(url=url, data=data)
    print(req.text)


if __name__ == '__main__':
    main()
