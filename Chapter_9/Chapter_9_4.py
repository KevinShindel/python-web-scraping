from requests import post


def main():
    """
    This function load files via form input
    page url: http://pythonscraping.com/files/form2.html
    :return:
    """
    files = {'uploadFile': open('logo.png', 'rb')}
    url = 'http://pythonscraping.com/../pages/files/processing2.php'
    req = post(url=url, files=files)
    print(req.text)


if __name__ == '__main__':
    main()
