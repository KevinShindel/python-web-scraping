from concurrent.futures import ThreadPoolExecutor
from os import getenv, cpu_count
from time import sleep
from typing import Union
from urllib.parse import urljoin

import geopandas as geopandas
import pymysql
from bs4 import BeautifulSoup as bs
from dateparser import parse
from dotenv import load_dotenv
from matplotlib import pyplot as plt
from pandas import read_sql
from pymysql import OperationalError, ProgrammingError
from requests import Session, get

cookie = None
connector = ...
cursor = ...
profile_urls = set()
session = Session()
DEFAULT_DATE = parse('1970-01-01 00:01:00')


def set_db():
    global connector, cursor
    connector = pymysql.connect(host='127.0.0.1', user='username', password='password')
    cursor = connector.cursor()
    cursor.execute(query='CREATE DATABASE IF NOT EXISTS  airgun;')
    cursor.execute('USE airgun;')
    rez = cursor.execute('''SELECT EXISTS(
                                    SELECT * FROM information_schema.tables 
                                    WHERE table_schema = 'airgun' 
                                    AND table_name = 'profile'
                    );''')
    if not rez:
        cursor.execute('''CREATE TABLE profile (
                                                id INT auto_increment PRIMARY KEY,
                                                username VARCHAR(128)  NOT NULL,
                                                registration_date datetime NULL,
                                                last_visit_date datetime NULL,
                                                total_messages INT NULL,
                                                user_group VARCHAR(512) NULL,
                                                country VARCHAR(256)  NULL,
                                                age INT NULL,
                                                site VARCHAR(512) NULL,
                                                interests VARCHAR(1024) NULL,
                                                ammo VARCHAR(1024) NULL,
                                                occupation VARCHAR(1024) NULL,
                                                sign VARCHAR(8096) NULL,
                                                profile_url VARCHAR(128)  NULL,
                                                lat DOUBLE NULL,
                                                lng DOUBLE NULL
                                                ) COLLATE utf8mb4_unicode_ci;''')
    else:
        cursor.execute('TRUNCATE profile;')


def close_db():
    connector.close()
    cursor.close()


def login():
    load_dotenv()
    username = getenv('AIRGUN_USERNAME', None)
    password = getenv('AIRGUN_PASSWORD', None)
    url = getenv('AIRGUN_URL', None)
    global cookie
    s = session.get(url=url+'ucp.php')
    sid = s.cookies.get_dict()['bb3_81a_sid']  # Set SID from cookies
    params = {'username': username, 'password': password,  # Full post form
              'autologin': 'on', 'redirect': 'index.php', 'sid': sid, 'login': 'Вход'}
    s = session.post(url+'ucp.php?mode=login', params)  # Send POST form
    cookie = s.cookies


def get_table_value(soup, string):
    try:
        return soup.find('td', string=string).parent.find_all('td')[1].get_text()
    except:
        return None


def parse_profile(url: str) -> [str]:
    sleep(0.25)
    base_url = getenv('AIRGUN_URL', None)
    url = urljoin(base_url+'memberlist.php', url)
    html = session.get(url=url, cookies=cookie)
    soup = bs(html.content, features='html.parser')
    return [
        soup.find('div', {'id': 'pagecontent'}).find('b', {'class': 'gen'}).get_text(), # username
        parse(get_table_value(soup, 'Зарегистрирован: ')) or DEFAULT_DATE, # registraion_date
        parse(get_table_value(soup, 'Последнее посещение: ')) or DEFAULT_DATE, # last_visit_Date
        soup.find('td', string='Всего сообщений: ').parent.find('b').get_text(), # total_messages
        get_table_value(soup, 'Группы: '), # user_group
        get_table_value(soup, 'Откуда: '), # country
        get_table_value(soup, 'Возраст: ') or 0, # age
        get_table_value(soup, 'Род занятий: '), # occupation
        get_table_value(soup, 'Интересы: '), # interests
        get_table_value(soup, 'Сайт: '), # site
        get_table_value(soup, 'Арсенал: '), # ammo
        soup.find("div", {'class': 'postbody'}).get_text() if soup.find_all("div", {'class': 'postbody'}) else None, # sign
        url, # profile_url
    ]


def save_profile(data: list):
    cursor.executemany("""INSERT INTO profile (username, registration_date, last_visit_date, total_messages,
                            user_group, country, age, occupation, interests, site, ammo, sign, profile_url) 
                            VALUES (%s, %s,  %s, %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s,  %s)""", data)
    connector.commit()


def collect_profile_pages(url: Union[str, bool]):
    html = session.get(url=url, cookies=cookie)
    soup = bs(html.content.decode('utf-8'), features='html.parser')
    next_page = soup.find_all(lambda tag: tag.name == "a" and "След." in tag.text) or False
    if next_page:
        next_page = urljoin(url, next_page[0].attrs['href'])

    for profile in soup.select("a[href*=viewprofile]"):
        profile_urls.add(profile['href'])

    print(f'find next page: {next_page}')
    sleep(0.25)
    if next_page:
        print(f'total urls: {len(profile_urls)}')
        collect_profile_pages(url=next_page)
    return len(profile_urls)


def main():
    """
    main method for run all logic
    :return: None
    """
    set_db()
    login()
    base_url = getenv('AIRGUN_URL', None)
    total_pages = collect_profile_pages(url=base_url+'memberlist.php')
    print(f'Find: {total_pages} total user profile pages')
    with ThreadPoolExecutor(max_workers=cpu_count()*2) as executor:
        data = executor.map(parse_profile, profile_urls)
    save_profile(list(data))
    close_db()


def get_coordinates():
    conn = pymysql.connect(host='127.0.0.1', user='username', password='password')
    cur = conn.cursor()
    cur.execute(query='SELECT DISTINCT(country) FROM airgun.profile WHERE country != "";')
    country = [item[0] for item in cur.fetchall()]
    with ThreadPoolExecutor(max_workers=cpu_count() * 4) as executor:
        data = executor.map(make_maps_request, country)
    for item in data:
        if not item:
            continue
        try:
            query = f"""UPDATE airgun.profile SET lat = {item["lat"]}, lng = {item["lng"]} WHERE country = '{item["country"]}'"""
            cur.execute(query=query)
            conn.commit()
        except (OperationalError, ProgrammingError):
            continue


def make_maps_request(city) -> {}:
    load_dotenv()
    key = getenv('GOOGLE_MAPS_KEY', None)
    url = f'https://maps.googleapis.com/maps/api/geocode/json?address={city}&key={key}'
    request = get(url=url)
    try:
        data = request.json()
        coords = data['results'][0]['geometry']['location']
        coords.update({'country': city})
        return coords
    except:
        return {}


def create_maps():
    world = geopandas.read_file(geopandas.datasets.get_path('naturalearth_lowres'))
    ua_map = world[world['name'] == 'Ukraine'].plot(color='lightblue', edgecolor='black')
    conn = pymysql.connect(host='127.0.0.1', user='username', password='password', db='airgun')
    df = read_sql(sql="""SELECT AVG(lat) as lat, AVG(lng) as lng, SUM(total_messages) as messages
                         FROM profile
                            WHERE country !='' 
                                AND 
                            lat IS NOT NULL 
                                AND 
                            lng IS NOT NULL 
                            GROUP BY country ORDER BY SUM(total_messages) DESC LIMIT 10;""", con=conn)

    gdf = geopandas.GeoDataFrame(
            df, geometry=geopandas.points_from_xy(df['lng'], df['lat']))
    gdf.plot(ax=ua_map, color='red')
    gdf.plot.scatter(x='lat', y='lng', s=gdf["messages"])
    plt.show()


if __name__ == '__main__':
    main()
    get_coordinates()
    create_maps()
