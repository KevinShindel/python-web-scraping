from bs4 import BeautifulSoup
from urllib.request import urlopen
url = 'http://bit.ly/1KGe2Qk'


def main(link: str = None):
    request = urlopen(url=link)
    html = BeautifulSoup(request, features='html.parser')
    item = html.find_all(lambda tag: len(tag.attrs) == 2, {'class': 'gift', 'id': 'gift1'})
    print(item)


if __name__ == '__main__':
    main(link=url)
