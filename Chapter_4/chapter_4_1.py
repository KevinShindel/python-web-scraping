from twitter import Twitter, OAuth
from os import getenv
from dotenv import load_dotenv


class TwitterWorker:
    load_dotenv()
    consumer_key = getenv('TWITTER_CONSUMER_KEY', None)
    consumer_secret = getenv('TWITTER_CONSUMER_SECRET', None)
    token = getenv('TWITTER_TOKEN', None)
    token_secret = getenv('TWITTER_TOKEN_SECRET', None)

    def __init__(self):
        self.api = Twitter(auth=OAuth(consumer_key=self.consumer_key,
                                      consumer_secret=self.consumer_secret,
                                      token=self.token,
                                      token_secret=self.token_secret)
                           )

    def search(self):
        request = self.api.search.tweets(q='#python', count=5)
        print(request)
        return request


if __name__ == '__main__':
    TwitterWorker().search()
