from urllib.request import urlopen
import json
from os import getenv
from dotenv import load_dotenv


def parse_ip(ip: str) -> dict:
    load_dotenv()
    access_key = getenv('API_STACK_ACCESS_KEY', None)
    """
    Retrieve IP and return info about IP
    :param ip: IP-addr
    :return: dict of info
    """
    url = f'http://api.ipstack.com/{ip}?access_key={access_key}'
    response = json.loads(urlopen(url=url).read())
    data = {
        'city': response['city'],
        'latitude': response['latitude'],
        'longitude': response['longitude']
    }
    return data


if __name__ == '__main__':
    print(parse_ip(ip='188.163.64.29'))
