import datetime
import json
import random
import re
from urllib.request import urlopen
from os import getenv
from dotenv import load_dotenv

from bs4 import BeautifulSoup as bs

random.seed(datetime.datetime.now())


def get_links(article_url: str) -> bs:
    html = urlopen('http://en.wikipedia.org'+article_url)
    soup = bs(html, features="html.parser")
    data = soup.find('div', {'id': 'bodyContent'}).find_all('a', href=re.compile('^(/wiki/)((?!:).)*$'))
    return data


def get_history_ip(page_url: str) -> set:
    page_url = page_url.replace('/wiki/', '')
    history_url = f'http://en.wikipedia.org/w/index.php?title={page_url}&action=history'
    print(f'history url: {history_url}')
    html = urlopen(history_url)
    soup = bs(html)
    ip_addreses = soup.find_all('a', class_='mw-anonuserlink')
    address_list = set(ip.get_text() for ip in ip_addreses)
    print(address_list)
    return address_list


def parse_ip(ip: str) -> dict:
    load_dotenv()
    access_key = getenv('API_STACK_ACCESS_KEY', None)

    """
    Retrieve IP and return info about IP
    :param ip: IP-addr
    :return: dict of info
    """
    url = f'http://api.ipstack.com/{ip}?access_key={access_key}'
    response = json.loads(urlopen(url=url).read())
    data = {
        'city': response['city'],
        'latitude': response['latitude'],
        'longitude': response['longitude']
    }
    return data


def main():
    links = get_links('/wiki/Python')
    while len(links):
        for link in links:
            print('-----------------')
            history_ip = get_history_ip(link.attrs['href'])
            for ip in history_ip:
                print(parse_ip(ip))

        new_link = links[random.randint(0, len(links) -1)].attrs['href']
        print(f'new random link: {new_link}')
        links = get_links(new_link)


if __name__ == '__main__':
    main()
