from PIL import Image
import pytesseract
import os


def optimize_image(image: str) -> str:
    img = Image.open(image)
    img = img.convert('L')
    img = img.point(lambda x: 0 if x < 100 else 255)
    file_name = f'normalized_{os.path.basename(image)}'
    path = os.path.join('./normalized', file_name)
    img.save(path)
    return path


def main():
    """
    OCR with simple optimization via Image
    :return: optimize image path
    """
    path_to_dir = 'data/images'
    image_list = list(map(lambda image_path: os.path.join(path_to_dir, image_path), os.listdir(path_to_dir)))
    for image_path in image_list:
        image = optimize_image(image=image_path)
        image = Image.open(image)
        output = pytesseract.image_to_string(image=image)
        print(output)


if __name__ == '__main__':
    main()
