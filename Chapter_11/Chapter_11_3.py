from PIL import Image
import pytesseract
from os import listdir, path
import numpy as np
import cv2
from os.path import basename


def numpy_optimize(image_path):
    """
    This method optimize image via cv2 and numpy
    :param image_path: path of image
    :return: optimized image path
    """
    img = cv2.imread(image_path)
    norm_img = np.zeros((img.shape[0], img.shape[1]))
    img_normalized = cv2.normalize(img, norm_img, 0, 255, cv2.NORM_MINMAX)
    img_thresholded = cv2.threshold(img_normalized, 100, 255, cv2.THRESH_BINARY)[1]
    img_blured = cv2.GaussianBlur(img_thresholded, (1, 1), 0)
    filename = f'optimized_{basename(image_path)}'
    cv2.imwrite(filename, img_blured)
    return filename


def main():
    """
    This function try to recognize image,
    if output is empty try to optimize via cv2
    :return:
    """
    first_example_list = list(map(lambda image_path: path.join('data/example', image_path), listdir('data/example')))
    second_example_list = list(map(lambda image_path: path.join('data/images', image_path), listdir('data/images')))
    full_list = first_example_list + second_example_list
    for image_path in full_list:
        image = Image.open(image_path)
        output = pytesseract.image_to_string(image=image)
        if not output:
            print(f'Cant read value for image: {image_path}, try to optimize')
            image = numpy_optimize(image_path)
            output = pytesseract.image_to_string(image=image)
        print(f'image path: {image_path}, result: {output}')


if __name__ == '__main__':
    main()
