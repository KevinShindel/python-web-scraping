from os.path import basename, join
from os import listdir
from pytesseract import image_to_boxes


def progress(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))
    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)
    print('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))


def ocr(image):
    captcha = image_to_boxes(image=image, lang='amz')
    return captcha


def create_box(ocr_result, filename):
    if not ocr_result:
        ocr_result = '0 0 0 0 0'
    with open(filename, 'w') as handler:
        handler.write(ocr_result)


def main():
    """
    Captcha recognition with OCR Tessetact and bypass captcha in post form
    :return:
    """
    SAVE_PATH = 'captcha_data'
    FILES_DIR = '/home/polkovnik/Downloads/captcha_data'

    files = list(map(lambda file: join(FILES_DIR, file), listdir(FILES_DIR)))
    for index, file in enumerate(files):
        filename = basename(file)
        box_filename = filename.replace('.tiff', '.box')
        ocr_box = ocr(file)
        create_box(ocr_box, join(SAVE_PATH, box_filename))
        progress(count=index, total=len(files))


if __name__ == '__main__':
    main()
