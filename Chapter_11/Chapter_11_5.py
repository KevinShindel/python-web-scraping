import datetime
import os
import re

from fake_useragent import UserAgent
import pytesseract
from os.path import basename, join
import cv2
import numpy as np
import time
from urllib.request import urlretrieve
from selenium import webdriver
from selenium.webdriver.common.by import By
from os import getenv
from dotenv import load_dotenv

CURRENT_FOLDER = os.path.curdir


def numpy_optimize(image_path):
    """
    This method optimize image via cv2 and numpy
    :param image_path: path of image
    :return: optimized image path
    """
    img = cv2.imread(image_path)
    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    # medianBlur = cv2.medianBlur(threshold, 3)

    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    norm_img = np.zeros((gray.shape[0], gray.shape[1]))
    img_normalized = cv2.normalize(gray, norm_img, 0, 255, cv2.NORM_MINMAX)
    img_thresholded = cv2.threshold(img_normalized, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    img_blured = cv2.GaussianBlur(img_thresholded, (1, 1), 0)
    # median = cv2.medianBlur(img_blured, 1)

    path = 'data/normalized'
    file_path = join(path, basename(image_path))
    cv2.imwrite(file_path, img_blured)
    return file_path


def recognize_image(image_path):
    img = cv2.imread(image_path)
    output = pytesseract.image_to_string(image=img, lang='eng', config='./trained_data')
    return output


def init_phantomjs_driver():
    load_dotenv()
    executable_path = getenv('PHATOM_JS_PATH', None)
    """
    Set PhantomJS headers for avoid robot detect
    :return:
    """
    user_agent = UserAgent().random
    headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
               'Accept-Language': 'zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3',
               'User-Agent': user_agent,
               'Connection': 'keep-alive'
               }

    for key, value in headers.items():
        webdriver.DesiredCapabilities.PHANTOMJS['phantomjs.page.customHeaders.{}'.format(key)] = value
    webdriver.DesiredCapabilities.PHANTOMJS['phantomjs.page.settings.userAgent'] = user_agent
    driver = webdriver.PhantomJS(executable_path=executable_path)
    # Set Webdriver windows dimension for correct CSS view
    driver.set_window_size(1366, 768)
    return driver


def main():
    # Get images from amazon  and read by OCR
    url = 'https://www.amazon.com/dp/1427030200'
    driver = init_phantomjs_driver()
    driver.get(url=url)
    time.sleep(2)
    driver.find_element(By.ID, 'sitbLogoImg').click()
    image_list = set()
    time.sleep(5)
    try:
        while 'pointer' in driver.find_element(By.ID, 'sitbReaderRightPageTurner').get_attribute('style'):
            print('Button for next page active, try to click...')
            driver.find_element(By.ID, 'sitbReaderRightPageTurner').click()
            time.sleep(5)
            pages = driver.find_elements(By.XPATH, "//div[@class='pageImage']/div/img")
            print(f'find: {len(pages)}')
            for p in pages:
                image = p.get_attribute('src')
                image_list.add(image)
    except:
        print('Error corrupted.. save screenshot')
        path = os.path.join(CURRENT_FOLDER, 'exception.png')
        driver.save_screenshot(filename=path)
    finally:
        driver.quit()

    for index, image in enumerate(sorted(image_list)):
        print(f'save {index} image url: {image}')
        name = clear_filename(image)
        path = os.path.join(CURRENT_FOLDER, 'war_and_peace', name)
        urlretrieve(url=image, filename=path)

    read_images()


def clear_filename(url):
    # Extract image name from url
    filename = datetime.datetime.now().strftime('%H_%M_%S.jpg')
    pattern = re.compile(r'/[\d.].*jpg')
    match = re.search(pattern, url)
    if match is not None:
        filename = match.group().replace('/', '')
    return filename


def read_images():
    # Create list of images , convert to GRAY and read by tesseract
    path = os.path.join(CURRENT_FOLDER, 'war_and_peace')
    image_list = list(map(lambda x: os.path.join(path, x), os.listdir(path)))
    for image in image_list:
        print(f'Current image to OCR: {image}')
        image = cv2.imread(image)
        image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        text = pytesseract.image_to_string(image)
        print(text)


if __name__ == '__main__':
    main()
