import pytesseract
from os import path, listdir
from os.path import basename, join
import cv2
import numpy as np


def numpy_optimize(image_path):
    """
    This method optimize image via cv2 and numpy
    :param image_path: path of image
    :return: optimized image path
    """
    img = cv2.imread(image_path)
    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    # medianBlur = cv2.medianBlur(threshold, 3)

    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    norm_img = np.zeros((gray.shape[0], gray.shape[1]))
    img_normalized = cv2.normalize(gray, norm_img, 0, 255, cv2.NORM_MINMAX)
    img_thresholded = cv2.threshold(img_normalized, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    img_blured = cv2.GaussianBlur(img_thresholded, (1, 1), 0)
    # median = cv2.medianBlur(img_blured, 1)

    path = 'data/normalized'
    file_path = join(path, basename(image_path))
    cv2.imwrite(file_path, img_blured)
    return file_path


def create_box_file(image_path):
    image_to_boxes = pytesseract.image_to_boxes(image=image_path, lang='eng', config='./trained_data')
    box_name = basename(image_path).split('.')[0] + '.box'
    path = 'data/normalized'
    with open(join(path, box_name), 'w') as handler:
        handler.write(image_to_boxes)


def recognize_image(image_path):
    img = cv2.imread(image_path)
    output = pytesseract.image_to_string(image=img, lang='eng', config='./trained_data')
    return output


def main():
    """
    This function get image and create a .box file
    :return:
    """
    first_example_list = list(map(lambda image_path: path.join('data/example', image_path), listdir('data/example')))
    second_example_list = list(map(lambda image_path: path.join('data/images', image_path), listdir('data/images')))
    full_list = first_example_list + second_example_list
    for image_path in full_list:
        image = numpy_optimize(image_path=image_path)
        create_box_file(image_path=image)
        output = recognize_image(image)
        print(f'Output: {output}')


if __name__ == '__main__':
    main()
