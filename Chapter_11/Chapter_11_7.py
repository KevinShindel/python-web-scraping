from urllib.parse import urljoin
from os import remove
from bs4 import BeautifulSoup as bs
from urllib.request import urlopen, urlretrieve
from requests import post
from numpy import zeros
from pytesseract import image_to_string
import cv2


def optimize_image(image):
    img_path, _ = urlretrieve(image, 'captcha.jpeg')
    img = cv2.imread(img_path)
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    norm_img = zeros((gray.shape[0], gray.shape[1]))
    img_normalized = cv2.normalize(gray, norm_img, 0, 255, cv2.NORM_MINMAX)
    img_thresholded = cv2.threshold(img_normalized, 100, 255, cv2.THRESH_BINARY)[1]
    img_blured = cv2.GaussianBlur(img_thresholded, (1, 1), 0)
    remove(img_path)
    return img_blured


def ocr(image):
    optimized = optimize_image(image)
    captcha = image_to_string(image=optimized, lang='captcha').replace(' ', '').replace('\n', '')
    if not captcha or len(captcha) < 5:
        raise Exception('Captcha not recognized')
    return captcha


def send_form(params):
    params['name'] = 'KevinShindel'
    params['subject'] = 'I come seek to Grail'
    params['comment_body[und][0][value]'] = '... and i\'m definitely not a robot.'
    post_url = params.pop('post_url')
    response = post(url=post_url, data=params)
    return response


def main():
    """
    Captcha recognition with OCR Tessetact and bypass captcha in post form
    :return:
    """
    params = {}
    url = 'http://www.pythonscraping.com/humans-only'
    html = urlopen(url=url)
    soup = bs(html, features='html.parser')
    captcha_location = soup.find('img', {'title': 'Image CAPTCHA'})['src']
    captcha_url = urljoin(url, captcha_location)
    form_url = soup.find('form', {'id': 'comment-form'})['action']
    params['form_build_id'] = soup.find('input', {'name': 'form_build_id'})['value']
    params['captcha_sid'] = soup.find('input', {'name': 'captcha_sid'})['value']
    params['captcha_token'] = soup.find('input', {'name': 'captcha_token'})['value']
    params['form_id'] = soup.find('input', {'name': 'form_id'})['value']
    params['post_url'] = urljoin(url, form_url)
    params['captcha_response'] = ocr(captcha_url)
    response = send_form(params)
    soup = bs(response.text, features='html.parser')
    message = soup.find('div', {'class': 'messages'})
    if message:
        print(f'Message: {message.get_text()}')
    else:
        print('Captcha not recognized!')


if __name__ == '__main__':
    main()
