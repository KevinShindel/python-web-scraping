from urllib.request import urlopen
from csv import DictReader
from numpy import zeros, asarray
from pytesseract import image_to_boxes
import cv2
from os.path import join


def progress(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))
    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)
    print('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))


def optimize_image(img):
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    norm_img = zeros((gray.shape[0], gray.shape[1]))
    img_normalized = cv2.normalize(gray, norm_img, 0, 255, cv2.NORM_MINMAX)
    img_thresholded = cv2.threshold(img_normalized, 100, 255, cv2.THRESH_BINARY)[1]
    img_blured = cv2.GaussianBlur(img_thresholded, (1, 1), 0)
    return img_blured


def get_image_from_url(url):
    response = urlopen(url)
    image = asarray(bytearray(response.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image


def ocr(image):
    try:
        captcha = image_to_boxes(image=image, lang='amz')
    except:
        captcha = image_to_boxes(image=image, lang='eng')
    return captcha


def create_box(ocr_result, filename):
    if not ocr_result:
        ocr_result = '0 25 20 50 70 0'
    with open(filename, 'w') as handler:
        handler.write(ocr_result)


def main():
    """
    Captcha recognition with OCR Tessetact and bypass captcha in post form
    :return:
    """
    SAVE_PATH = 'captcha_data'

    with open('item_lookup_captchalog.csv', 'r') as handler:
        data = DictReader(handler)
        urls = [row['url'] for row in data]
    for index, url in enumerate(urls):
        image = get_image_from_url(url)
        optimized_img = optimize_image(image)
        filename = f'{index}'.zfill(6) + '.tiff'
        box_filename = filename.replace('.tiff', '.box')
        cv2.imwrite(join(SAVE_PATH, filename), optimized_img)
        oprimized_ocr = ocr(optimized_img)
        create_box(oprimized_ocr, join(SAVE_PATH, box_filename))
        progress(count=index, total=len(urls))


if __name__ == '__main__':
    main()
