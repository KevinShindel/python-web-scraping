from PIL import Image
import pytesseract
import os


def main():
    """
    Simple OCR recognition without image normalize
    :return:
    """
    path_to_dir = 'data/images'
    image_list = list(map(lambda image_path: os.path.join(path_to_dir, image_path), os.listdir(path_to_dir)))
    for image_path in image_list:
        image = Image.open(image_path)
        output = pytesseract.image_to_string(image=image)
        print(output)


if __name__ == '__main__':
    main()
