import os
from urllib.request import urlretrieve, urlopen
from urllib.parse import urljoin
from pytesseract.pytesseract import TesseractError
from pytesseract import image_to_boxes
from PIL import Image, ImageOps
from bs4 import BeautifulSoup as bs
from time import sleep, perf_counter


class CaptchaProcessing:
    """
    This class provide fully functional for prepare images for training OCR systems
    """

    SOURCE_PATH = "captcha_data"

    def __init__(self):
        self.url = 'http://www.pythonscraping.com/humans-only'
        self.captcha_qty = 100
        self.throttling_time = 0.5
        # Select captcha for training
        # url = 'http://captcha.ru/captchas/multiwave/' Multiwave Captcha
        # url = 'http://captcha.ru/captchas/multiswirl/' MultiSwirl Captcha
        # url = 'http://captcha.ru/' KCAPTCHA

    def check_dirs(self):
        print('[?] Check dirs...')
        if not os.path.exists(self.SOURCE_PATH):
            os.mkdir(self.SOURCE_PATH)
        print('[+] All dirs checked...')

    def collect_images(self):
        print('[?] Begin collect captcha from site...')
        start = perf_counter()
        for index in range(self.captcha_qty):
            sleep(self.throttling_time)
            filename = self.generate_filename(self.SOURCE_PATH)
            html = urlopen(url=self.url)
            soup = bs(html, features='html.parser')
            captcha_url = soup.find('img', {'title': 'Image CAPTCHA'})['src']
            captcha_url = urljoin(self.url, captcha_url)
            urlretrieve(captcha_url, filename=filename)
            print(f'[?] Completed: {index} / {self.captcha_qty}')
        done = round(perf_counter() - start, 2)
        print(f'[+] All captcha downloaded. Time spend: {done} sec')

    def generate_filename(self, path):
        counter = os.listdir(path)
        counter = str(len(counter)).zfill(len(str(self.captcha_qty)))
        filename = f'{counter}.tiff'
        filename = os.path.join(path, filename)
        return filename

    def optimize_images(self):
        start = perf_counter()
        print('[?] Begin optimize images')
        file_list = os.listdir(self.SOURCE_PATH)
        file_list = list(map(lambda x: os.path.join(self.SOURCE_PATH, os.path.basename(x)), file_list))
        for index, image_path in enumerate(file_list):
            image = Image.open(image_path)
            image = image.convert('L')
            image = image.point(lambda x: 0 if x < 143 else 255)
            border_image = ImageOps.expand(image, border=20, fill='white')
            border_image.save(image_path)
            print(f'[?] Completed: {index} / {len(file_list)}')
        done = round(perf_counter() - start, 2)
        print(f'[+] All images optimized. Time spend: {done}')

    def save_box_file(self, image: str, box: str):
        ocr_filename = os.path.basename(image)
        filename, extension = ocr_filename.split('.')
        box_filename = f'{filename}.box'
        box_path = os.path.join(self.SOURCE_PATH, box_filename)
        with open(box_path, 'w') as handler:
            handler.write(box)

    def create_boxes(self):
        start = perf_counter()
        print('[?] Creating box files...')
        file_list = os.listdir(self.SOURCE_PATH)
        file_list = list(map(lambda x: os.path.join(self.SOURCE_PATH, os.path.basename(x)), file_list))
        for index, image_path in enumerate(file_list):
            try:
                box_output = image_to_boxes(image=image_path, lang='eng')
                self.save_box_file(image=image_path, box=box_output)
                print(f'[?] Completed {index} / {len(file_list)}')
            except TesseractError:
                print(f'Error corrupted while read: {image_path}, file removed')
                os.remove(image_path)
        done = round(perf_counter() - start, 2)
        print(f'[+] All box files created. Time spend: {done} sec.')

    @staticmethod
    def generate_training_data():
        start = perf_counter()
        print('[?] OCR training process initiated...')
        from Chapter_11.trainer import TesseractTrainer as tt
        tt().runAll()
        done = round(perf_counter() - start, 2)
        print(f'[+] OCR train complete. Time spend: {done} sec.')

    def process(self):
        self.check_dirs()
        self.collect_images()
        self.optimize_images()
        self.create_boxes()
        self.generate_training_data()


if __name__ == '__main__':
    CaptchaProcessing().process()
