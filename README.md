# Python Web Scraping

#### Chapter 1
- Basic usage urlopen  (from  urllib.request library)
- Basic usage BeautifulSoup
- Catch Exceptions (HTTPError, AttributeError) 

#### Chapter 2
- Search HTML elements by direct tag 
- Search elements by lambda func

#### Chapter 3
- Search HTML elements by RegEx in BS4 find method
- Entering in random library (show up a random seed method)
- Basic usage a set 
- Usage a global variables
- Extended usage of RegEx , optimize methods, tipization variables 
- Basic usage Scrapy 

#### Chapter 4
- Basic usage Twitter library (search twitters)
- Usage IPStack API by secret key (geoip)
- Crawl on Wikipedia and resolve an anonymous IP addr by IPStack API

#### Chapter 5
- Basic usage of urlretrieve from (urllib.request) library
- Usage DictWriter from csv library
- Write data from HTML to csv file
- Basic usage of SQL connector
- Crawl on Wikipedia and store data into SQL database via connector
- Recursive method for load data into SQL

#### Chapter 6
- Retrieve charset from meta tag and encoding HTML text
- On memory read csv file without downloading (StringIO and DictReader)
- Read PDF files with PyPDF2 library
- On memory read docx file without downloading with BytesIO and ZipFile and BS4

#### Chapter 7
- Entering an ngrams and OrderingDict

#### Chapter 8
- Basic text analyzing
- Moderate text analyzing
- Text encode convertation with bytes
- Entering into Markov chains
- Recursive searching via DB
- Acquaintance with NTLK library

#### Chapter 9
- Acquaintance with request library and auth by HTML form
- Auth into IPStack via POST form with credentials
- Auth into KIBANA via credentials
- Upload file via post form
- Acquaintance with Session to login with cookies
- Moderate login with cookie into Airgun site and collect users profiles

#### Chapter 10
- Acquaintance with PhantomJS and Selenium
- Acquaintance with locators (By), expected_conditions, WebDriverWait
- Acquaintance with StaleElementReferenceException

#### Chapter 11
- Acquaintance with OCR Tesseract (need to install tesseract before using)
```bash
sudo apt install tesseract-ocr libtesseract-dev -y 
``` 
- Image normalize with Image from Pillow library
- Image normalize with cv2
- Deep Image normalize with cv2 and numpy
- Get Amazon preview with FakeUserAgent, OCR, cv2
- Tesseract captcha basic training
- Basic captcha recognizing and bypass robot check
- Moderate training with parse CSV urls and create box files
- Main Training OCR method for generate trained data

#### Chapter 12
- Basic FakeUserAgent verification
- Avoid not visible elements on page

#### Chapter 13
- Basic Unit test
- Moderate Unit test
- Unit test on BS4
- Unit test on PhantomJS
- Acquaintance int Selenium Actions chains
- Drag & Drop method by Selenium (only Chrome driver) 

