import datetime
import random
import re
from urllib.request import urlopen

import pymysql
from bs4 import BeautifulSoup as bs

random.seed(datetime.datetime.now())
pages = set()
connection = pymysql.connect(host='127.0.0.1', user='username', password='password', db='wikipedia')
cur = connection.cursor()
cur.execute('USE wikipedia;')


def insert_page(url: str):
    cur.execute(f"SELECT * FROM pages WHERE url = '{url}'")
    if not cur.rowcount:
        cur.execute(f"INSERT INTO pages (url) VALUES ('{url}')")
        cur.connection.commit()
        return cur.lastrowid
    else:
        return cur.fetchone()[0]


def insert_link(frop_page_id: int, to_page_id: int):
    cur.execute(f"SELECT * FROM links WHERE from_page_id = {frop_page_id} AND to_page_id = {to_page_id}")
    if not cur.rowcount:
        cur.execute(f'INSERT INTO links (from_page_id, to_page_id) VALUES ({frop_page_id}, {to_page_id})')
        cur.connection.commit()


def get_links(url: str, recursion_lvl: int):
    global pages
    if recursion_lvl > 4:
        return
    else:
        from_page_id = insert_page(url)
        html = urlopen(f'http://en.wikipedia.org/{url}')
        soup = bs(html, features='html.parser')
        for link in soup.find_all('a', {'href': re.compile("^(/wiki/)((?!:).)*$")}):
            insert_link(frop_page_id=from_page_id, to_page_id=insert_page(link.attrs['href']))
            if link.attrs['href'] not in pages:
                new_page = link.attrs['href']
                pages.add(new_page)
                print(f'Total pages: {len(pages)}')
        new_page = list(pages)[random.randint(0, len(pages)+1)]
        print(new_page)
        get_links(url=new_page, recursion_lvl=recursion_lvl+1)


def main():
    try:
        get_links(url='/wiki/Kevin_Bacon', recursion_lvl=0)
    finally:
        cur.close()
        connection.close()


if __name__ == '__main__':
    main()
