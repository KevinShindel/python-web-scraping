import datetime
import random
import re
from urllib.request import urlopen

import pymysql
from bs4 import BeautifulSoup as bs

random.seed(datetime.datetime.now())
connection = pymysql.connect(host='127.0.0.1', user='username', password='password', db='scraping')
cur = connection.cursor()
cur.execute('USE scraping;')


def store(title, content):
    cur.execute(fr'INSERT INTO pages (title, content) VALUES("""{title}""", """{content}""");')
    cur.connection.commit()


def normalize(string: str):
    return re.sub('[\[\]\*\.]', '', string)


def get_links(url):
    html = urlopen(f'http://en.wikipedia.org/{url}')
    soup = bs(html, features='html.parser')
    title = soup.find('h1', {'id': 'firstHeading'}).get_text()
    title = normalize(title)
    content = soup.find('div', {'id': 'mw-content-text'}).find_all('p')[1].get_text()
    content = normalize(content)
    store(title=title, content=content)
    return soup.find('div', {'id': 'bodyContent'}).find_all('a', {'href': re.compile("^(/wiki/)((?!:).)*$")})


def main():
    links = get_links(url='/wiki/Kevin_Bacon')
    try:
        while len(links):
            new_article = links[random.randint(0, len(links) - 1)].attrs['href']
            print(new_article)
            links = get_links(url=new_article)
    finally:
        cur.close()
        connection.close()


if __name__ == '__main__':
    main()
