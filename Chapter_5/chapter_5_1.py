from urllib.request import urlopen, urlretrieve
import os
from bs4 import BeautifulSoup as bs

download_dir = 'downloaded'
base_url = 'http://pythonscraping.com'


def get_abs_url(base_url: str, source: str) -> str or None:
    if source.startswith('http://www.'):
        url = f'http://{source[11:]}'
    elif source.startswith('http://') or source.startswith('https://'):
        url = source
    elif source.startswith('www.'):
        url = f'http://{source}'
    else:
        url = f'{base_url}/{source}'
    if base_url not in url:
        return None
    return url


def get_download_path(download_dir, file_name):
    file_path = os.path.join(download_dir, file_name)
    if not os.path.exists(download_dir):
        os.makedirs(download_dir)
    return file_path


def main():
    html = urlopen('http://www.pythonscraping.com')
    soup = bs(html, features='html.parser')
    download_list = soup.find_all('img', src=True)
    for item in download_list:
        file_url = get_abs_url(base_url, item['src'])
        if file_url is not None:
            print(file_url)
            file_name = file_url.split('/')[-1]
            urlretrieve(file_url, get_download_path(download_dir, file_name))


if __name__ == '__main__':
    main()
