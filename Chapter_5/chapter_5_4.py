import pymysql


def main():
    connection = pymysql.connect(host='127.0.0.1', user='username', password='password', db='scraping')
    cur = connection.cursor()
    cur.execute('USE scraping;')
    cur.execute('DESCRIBE pages;')
    print(cur.fetchone())

    # cur.execute('INSERT INTO pages (title, content) VALUES("some new title", "some news contrents");')
    # cur.execute('SELECT * FROM pages;')
    # print(cur.fetchone())
    cur.close()
    connection.close()


if __name__ == '__main__':
    main()
