import csv
from urllib.request import urlopen
from bs4 import BeautifulSoup as bs


def main():
    url = 'http://en.wikipedia.org/wiki/Comparison_of_text_editors'
    html = urlopen(url)
    soup = bs(html, features='html.parser')
    table = soup.find('table', class_='wikitable')
    rows = table.find_all('tr')
    csv_file = open('test.csv', 'wt')
    try:
        writer = csv.writer(csv_file)
        for row in rows:
            csv_row = []
            for cell in row.find_all(['td', 'th']):
                csv_row.append(cell.get_text())
            writer.writerow(csv_row)
    except:
        pass
    finally:
        csv_file.close()


if __name__ == '__main__':
    main()
