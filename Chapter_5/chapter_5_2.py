from csv import DictWriter


def main():
    csv_file = open('test.csv', 'wt')
    try:
        headers = ['some', 'header', 'here']
        writer = DictWriter(csv_file, fieldnames=headers)
        writer.writeheader()
        for i in range(10):
            writer.writerow({'some': i, 'header': i+2, 'here': i**2})
    except:
        pass
    finally:
        csv_file.close()


if __name__ == '__main__':
    main()
