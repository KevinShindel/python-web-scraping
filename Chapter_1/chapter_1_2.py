from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.error import HTTPError


def main():
    try:
        url = 'http://www.pythonscraping.com/pagespage1.html'
        html = urlopen(url=url)
        bs4 = BeautifulSoup(html.read(), features="html.parser")
        print(bs4.body.h1)
    except HTTPError as Err:
        print(Err)
    else:
        pass

if __name__ == '__main__':
    main()
