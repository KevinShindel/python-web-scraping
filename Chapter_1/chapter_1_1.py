from urllib.request import urlopen


def main():
    """
    This Scraper basicly use urlopen and print html code
    :return:
    """
    url = 'http://www.pythonscraping.com/pages/page1.html'
    html = urlopen(url=url)
    print(html.read())


if __name__ == '__main__':
    main()
