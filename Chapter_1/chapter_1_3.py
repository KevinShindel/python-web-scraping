from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.error import HTTPError


def main(url_link: str = None):
    try:
        html = urlopen(url=url_link)
    except HTTPError:
        return None
    try:
        bs4 = BeautifulSoup(html.read(), features="html.parser")
        title = bs4.body.h1
    except AttributeError as Err:
        return None
    else:
        return title


if __name__ == '__main__':
    url = 'http://www.pythonscraping.com/pages/page1.html'
    print(main(url_link=url))
